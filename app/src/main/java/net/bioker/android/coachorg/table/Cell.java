package net.bioker.android.coachorg.table;

import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;

public class Cell {

    private int number;
    private TextView textView;
    private Row origin;

    public Cell(int number, Row origin) {
        this.number = number;
        this.origin = origin;
        textView = new TextView(origin.getContext());
        textView.setPadding(2, 2, 2, 2);
        textView.setBackgroundColor(Color.WHITE);
        textView.setGravity(Gravity.CENTER);
    }

    public int getNumber() {
        return number;
    }

    public TextView getTextView() {
        return textView;
    }

    public Row getOrigin() {
        return origin;
    }
}
