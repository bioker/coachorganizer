package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Group;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBGroup {

    public static final String TABLE_NAME = "myGroup";
    public static final String ID_FIELD_NAME = "id";
    public static final String NAME_FIELD_NAME = "name";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            NAME_FIELD_NAME + " TEXT NOT NULL);";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Group group) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_FIELD_NAME, group.getName());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Group group) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_FIELD_NAME, group.getName());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + group.getId(), new String[]{}) > 0;
    }

    public static Group get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
            Group result = new Group(id, name);
            Log.d("DB", "group found:\n" + result);
            return result;
        } else {
            Log.w("DB", "group with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Group> getAll(SQLiteDatabase db) {
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<Group> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
                Group group = new Group(id, name);
                Log.d("DB", "group found:\n" + result);
                result.add(group);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find group");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerName = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerName.getTextView().setText(NAME_FIELD_NAME);

        ArrayList<Group> groups = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Group group : groups) {
            Row exerciseRow = table.addRow();

            Cell rowId = exerciseRow.addCell();
            Cell rowName = exerciseRow.addCell();

            rowId.getTextView().setText(String.valueOf(group.getId()));
            rowName.getTextView().setText(group.getName());
        }

    }
}
