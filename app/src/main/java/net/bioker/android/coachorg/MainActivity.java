package net.bioker.android.coachorg;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import net.bioker.android.coachorg.fragments.DebugFragment;
import net.bioker.android.coachorg.fragments.EducableFragment;
import net.bioker.android.coachorg.fragments.PaymentFragment;
import net.bioker.android.coachorg.fragments.PresenceFragment;
import net.bioker.android.coachorg.fragments.ScheduleFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout menuLayout;
    private ListView menuListView;
    private ArrayAdapter<String> menuAdapter;
    private ArrayList<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menuLayout = (DrawerLayout) findViewById(R.id.mainRootLayout);

        fragments = new ArrayList<>();

        fragments.add(new ScheduleFragment());
        fragments.add(new PresenceFragment());
        fragments.add(new EducableFragment());
        fragments.add(new PaymentFragment());
        fragments.add(new DebugFragment());

        setupMenu();
    }

    private void setupMenu() {
        menuListView = (ListView) findViewById(R.id.menuList);
        String[] mArray = getResources().getStringArray(R.array.menu);
        menuAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mArray);
        menuListView.setAdapter(menuAdapter);

        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.mainContainer, fragments.get(position));
                transaction.commit();
                menuLayout.closeDrawer(menuListView);
            }
        });
    }

    private void showInputDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Title");
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, input.getText().toString(), Toast.LENGTH_SHORT);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
