package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Educable;
import net.bioker.android.coachorg.model.Payment;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBPayment {

    public static final String TABLE_NAME = "payment";
    public static final String ID_FIELD_NAME = "id";
    public static final String EDUCABLE_FIELD_NAME = "educable";
    public static final String DATE_FIELD_NAME = "paymentDate";
    public static final String PAYMENT_FIELD_NAME = "payment";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            EDUCABLE_FIELD_NAME + " INTEGER NOT NULL, " +
            DATE_FIELD_NAME + " TEXT NOT NULL, " +
            PAYMENT_FIELD_NAME + " FLOAT NOT NULL, " +
            "FOREIGN KEY(" + EDUCABLE_FIELD_NAME + ") REFERENCES " +
            DBEducable.TABLE_NAME + "(" + DBEducable.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Payment payment) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, payment.getEducable());
        contentValues.put(DATE_FIELD_NAME, payment.getDate());
        contentValues.put(PAYMENT_FIELD_NAME, payment.getPayment());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Payment payment) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, payment.getEducable());
        contentValues.put(DATE_FIELD_NAME, payment.getDate());
        contentValues.put(PAYMENT_FIELD_NAME, payment.getPayment());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + payment.getId(), new String[]{}) > 0;
    }

    public static Payment get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
            String date = cursor.getString(cursor.getColumnIndex(DATE_FIELD_NAME));
            float payment = cursor.getFloat(cursor.getColumnIndex(PAYMENT_FIELD_NAME));
            Payment result = new Payment(id, educable, date, payment);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Payment> getAll(SQLiteDatabase db, String selection) {
        Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, null);
        ArrayList<Payment> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
                String date = cursor.getString(cursor.getColumnIndex(DATE_FIELD_NAME));
                float paymentValue = cursor.getFloat(cursor.getColumnIndex(PAYMENT_FIELD_NAME));
                Payment payment = new Payment(id, educable, date, paymentValue);
                Log.d("DB", "exercise found:\n" + result);
                result.add(payment);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static ArrayList<Payment> getAll(SQLiteDatabase db) {
        return getAll(db, null);
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerEducable = header.addCell();
        Cell headerDate = header.addCell();
        Cell headerPayment = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerEducable.getTextView().setText(EDUCABLE_FIELD_NAME);
        headerDate.getTextView().setText(DATE_FIELD_NAME);
        headerPayment.getTextView().setText(PAYMENT_FIELD_NAME);

        ArrayList<Payment> payments = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Payment payment : payments) {
            Row paymentRow = table.addRow();

            Cell rowId = paymentRow.addCell();
            Cell rowEducable = paymentRow.addCell();
            Cell rowDate = paymentRow.addCell();
            Cell rowPayment = paymentRow.addCell();

            rowId.getTextView().setText(String.valueOf(payment.getId()));
            rowEducable.getTextView().setText(String.valueOf(payment.getEducable()));
            rowDate.getTextView().setText(payment.getDate());
            rowPayment.getTextView().setText(String.valueOf(payment.getPayment()));
        }

    }

    public static String getEducablePayment(SQLiteDatabase db, Educable educable, int month) {
        ArrayList<Payment> payments = getAll(
                db,
                "replace(strftime('%m'," + DATE_FIELD_NAME + "),'0','')='" + month + "'" +
                        " AND " + EDUCABLE_FIELD_NAME + "=" + educable.getId()
        );
        if (payments.size() < 1) {
            Log.w("DB", "payment for educable " + educable.getId() + " in mounth " + month + " not found");
            return "";
        }
        if (payments.size() > 1) {
            Log.w("DB", "few payment per mounth found - educable " + educable.getId() + " month " + month);
        }
        return String.valueOf(payments.get(0).getPayment());
    }
}
