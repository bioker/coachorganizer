package net.bioker.android.coachorg.model;

public class Educable {

    private int id;
    private int group;
    private String name;

    public Educable(int id, int group, String name) {
        this.id = id;
        this.group = group;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }
}
