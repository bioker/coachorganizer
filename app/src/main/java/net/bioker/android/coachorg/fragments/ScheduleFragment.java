package net.bioker.android.coachorg.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;

import net.bioker.android.coachorg.R;
import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.table.Table;

import java.text.ParseException;
import java.util.Calendar;

public class ScheduleFragment extends Fragment {

    private DBConnector dbConnector;
    private DatePicker datePicker;
    private FrameLayout tableContainer;
    private Table table;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_schedule, null);
        dbConnector = new DBConnector(v.getContext());
        datePicker = (DatePicker) v.findViewById(R.id.scheduleDatePicker);
        tableContainer = (FrameLayout) v.findViewById(R.id.scheduleTableContainer);

        table = new Table(v.getContext());
        table.addTable(tableContainer);

        setupDatePicker();

        return v;
    }

    private void setupDatePicker() {
        Calendar today = Calendar.getInstance();

        datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
                today.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                        String date = year + "-" + String.valueOf(monthOfYear + 1) + "-" + dayOfMonth;
                        Log.d("SCHEDULE", "date - " + date);
                        updateTable(date);
                    }
                });
    }

    private void updateTable(String date) {
        table.clear();
        try {
            DBConnector.fillScheduleTable(
                    dbConnector.getReadableDatabase(),
                    table,
                    date);
        } catch (ParseException e) {
            Log.e("SCHEDULE", "parse exception for date - " + date);
        }
    }
}
