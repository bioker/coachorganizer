package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Group;
import net.bioker.android.coachorg.model.Lesson;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DBLesson {

    public static final String TABLE_NAME = "lesson";
    public static final String ID_FIELD_NAME = "id";
    public static final String GROUP_FIELD_NAME = "myGroup";
    public static final String DATE_FIELD_NAME = "lessonDate";
    public static final String BEGIN_FIELD_NAME = "begin";
    public static final String END_FIELD_NAME = "end";
    public static final String PRICE_FIELD_NAME = "price";
    public static final String NOTE_FIELD_NAME = "note";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            GROUP_FIELD_NAME + " INTEGER NOT NULL, " +
            DATE_FIELD_NAME + " TEXT NOT NULL, " +
            BEGIN_FIELD_NAME + " TEXT NOT NULL, " +
            END_FIELD_NAME + " TEXT NOT NULL, " +
            PRICE_FIELD_NAME + " FLOAT, " +
            NOTE_FIELD_NAME + " TEXT, " +
            "FOREIGN KEY(" + GROUP_FIELD_NAME + ") REFERENCES " +
            DBGroup.TABLE_NAME + "(" + DBGroup.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Lesson lesson) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_FIELD_NAME, lesson.getGroup());
        contentValues.put(DATE_FIELD_NAME, lesson.getDate());
        contentValues.put(BEGIN_FIELD_NAME, lesson.getBegin());
        contentValues.put(END_FIELD_NAME, lesson.getEnd());
        contentValues.put(PRICE_FIELD_NAME, lesson.getPrice());
        contentValues.put(NOTE_FIELD_NAME, lesson.getNote());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Lesson lesson) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_FIELD_NAME, lesson.getGroup());
        contentValues.put(DATE_FIELD_NAME, lesson.getDate());
        contentValues.put(BEGIN_FIELD_NAME, lesson.getBegin());
        contentValues.put(END_FIELD_NAME, lesson.getEnd());
        contentValues.put(PRICE_FIELD_NAME, lesson.getPrice());
        contentValues.put(NOTE_FIELD_NAME, lesson.getNote());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + lesson.getId(), new String[]{}) > 0;
    }

    public static Lesson get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int group = cursor.getInt(cursor.getColumnIndex(GROUP_FIELD_NAME));
            String date = cursor.getString(cursor.getColumnIndex(DATE_FIELD_NAME));
            String begin = cursor.getString(cursor.getColumnIndex(BEGIN_FIELD_NAME));
            String end = cursor.getString(cursor.getColumnIndex(END_FIELD_NAME));
            float price = cursor.getFloat(cursor.getColumnIndex(PRICE_FIELD_NAME));
            String note = cursor.getString(cursor.getColumnIndex(NOTE_FIELD_NAME));
            Lesson result = new Lesson(id, group, date, begin, end, price, note);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Lesson> getAll(SQLiteDatabase db) {
        return getAll(db, null);
    }

    public static ArrayList<Lesson> getAll(SQLiteDatabase db, String selection) {
        Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, null);
        ArrayList<Lesson> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int group = cursor.getInt(cursor.getColumnIndex(GROUP_FIELD_NAME));
                String date = cursor.getString(cursor.getColumnIndex(DATE_FIELD_NAME));
                String begin = cursor.getString(cursor.getColumnIndex(BEGIN_FIELD_NAME));
                String end = cursor.getString(cursor.getColumnIndex(END_FIELD_NAME));
                float price = cursor.getFloat(cursor.getColumnIndex(PRICE_FIELD_NAME));
                String note = cursor.getString(cursor.getColumnIndex(NOTE_FIELD_NAME));
                Lesson lesson = new Lesson(id, group, date, begin, end, price, note);
                Log.d("DB", "exercise found:\n" + result);
                result.add(lesson);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static ArrayList<Lesson> getLessonByGroup(SQLiteDatabase db, Group group) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return getLessonByGroup(db, group, dateFormat.format(new Date()));
    }

    public static ArrayList<String> getDistinctBeginTimeByDate(
            SQLiteDatabase db, String beginDate, String endDate) {
        Cursor cursor = db.rawQuery("SELECT DISTINCT " + BEGIN_FIELD_NAME + " FROM " + TABLE_NAME +
                " WHERE strftime('%s'," + DATE_FIELD_NAME + ")>=strftime('%s','" + beginDate + "')" +
                " AND strftime('%s'," + DATE_FIELD_NAME + ")<=strftime('%s','" + endDate + "')", null);
        ArrayList<String> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Log.d("DB", "found lesson begin time - " + cursor.getString(0));
                result.add(cursor.getString(0));
            } while (cursor.moveToNext());
        } else {
            Log.d("DB", "lessons between dates - " + beginDate + " and " + endDate + " not found");
        }
        return result;
    }

    public static ArrayList<Group> getGroupsByDateAndTime(
            SQLiteDatabase db, String lessonDate, String beginTime, String endTime) {
        String query = "SELECT " + GROUP_FIELD_NAME + " FROM " + TABLE_NAME +
                " WHERE " + DATE_FIELD_NAME + "='" + lessonDate + "'" +
                " AND " + BEGIN_FIELD_NAME + "='" + beginTime + "'" +
                " AND strftime('%s'," + END_FIELD_NAME + ")<=strftime('%s','" + endTime + "')";
        Log.d("DB", "query:\n" + query);
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<Group> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Log.d("DB", "found group - " + cursor.getString(0));
                Group group = DBGroup.get(db, cursor.getInt(0));
                result.add(group);
            } while (cursor.moveToNext());
        } else {
            Log.d("DB",
                    "lessons with date - " + lessonDate + ", between times - "
                            + beginTime + " and " + endTime + " not found");
        }
        return result;
    }

    public static ArrayList<Lesson> getLessonByGroup(SQLiteDatabase db, Group group, String date) {
        ArrayList<Lesson> result = getAll(
                db,
                "strftime('%Y%m'," + DATE_FIELD_NAME + ")=strftime('%Y%m','" + date + "')" +
                        " AND " + GROUP_FIELD_NAME + "=" + group.getId()
        );
        if (result.size() < 1) {
            Log.w("DB", "lessons for group " + group.getId() + " by date " + date + " not found");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerGroup = header.addCell();
        Cell headerDate = header.addCell();
        Cell headerBegin = header.addCell();
        Cell headerEnd = header.addCell();
        Cell headerPrice = header.addCell();
        Cell headerNote = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerGroup.getTextView().setText(GROUP_FIELD_NAME);
        headerDate.getTextView().setText(DATE_FIELD_NAME);
        headerBegin.getTextView().setText(BEGIN_FIELD_NAME);
        headerEnd.getTextView().setText(END_FIELD_NAME);
        headerPrice.getTextView().setText(PRICE_FIELD_NAME);
        headerNote.getTextView().setText(NOTE_FIELD_NAME);

        ArrayList<Lesson> lessons = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Lesson lesson : lessons) {
            Row lessonRow = table.addRow();

            Cell rowId = lessonRow.addCell();
            Cell rowGroup = lessonRow.addCell();
            Cell rowDate = lessonRow.addCell();
            Cell rowBegin = lessonRow.addCell();
            Cell rowEnd = lessonRow.addCell();
            Cell rowPrice = lessonRow.addCell();
            Cell rowNote = lessonRow.addCell();

            rowId.getTextView().setText(String.valueOf(lesson.getId()));
            rowGroup.getTextView().setText(String.valueOf(lesson.getGroup()));
            rowDate.getTextView().setText(lesson.getDate());
            rowBegin.getTextView().setText(lesson.getBegin());
            rowEnd.getTextView().setText(lesson.getEnd());
            rowPrice.getTextView().setText(String.valueOf(lesson.getPrice()));
            rowNote.getTextView().setText(lesson.getNote());
        }

    }
}
