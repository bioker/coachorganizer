package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Presence;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBPresence {

    public static final String TABLE_NAME = "presence";
    public static final String ID_FIELD_NAME = "id";
    public static final String EDUCABLE_FIELD_NAME = "educable";
    public static final String LESSON_FIELD_NAME = "lesson";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            EDUCABLE_FIELD_NAME + " INTEGER NOT NULL, " +
            LESSON_FIELD_NAME + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + EDUCABLE_FIELD_NAME + ") REFERENCES " +
            DBEducable.TABLE_NAME + "(" + DBEducable.ID_FIELD_NAME + ")," +
            "FOREIGN KEY(" + LESSON_FIELD_NAME + ") REFERENCES " +
            DBLesson.TABLE_NAME + "(" + DBLesson.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Presence presence) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, presence.getEducable());
        contentValues.put(LESSON_FIELD_NAME, presence.getLesson());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Presence presence) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, presence.getEducable());
        contentValues.put(LESSON_FIELD_NAME, presence.getLesson());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + presence.getId(), new String[]{}) > 0;
    }

    public static Presence get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
            int lesson = cursor.getInt(cursor.getColumnIndex(LESSON_FIELD_NAME));
            Presence result = new Presence(id, educable, lesson);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Presence> getAll(SQLiteDatabase db) {
        return getAll(db, null);
    }

    public static ArrayList<Presence> getAll(SQLiteDatabase db, String selection) {
        Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, null);
        ArrayList<Presence> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
                int lesson = cursor.getInt(cursor.getColumnIndex(LESSON_FIELD_NAME));
                Presence presence = new Presence(id, educable, lesson);
                Log.d("DB", "exercise found:\n" + result);
                result.add(presence);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerEducable = header.addCell();
        Cell headerLesson = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerEducable.getTextView().setText(EDUCABLE_FIELD_NAME);
        headerLesson.getTextView().setText(LESSON_FIELD_NAME);

        ArrayList<Presence> presences = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Presence presence : presences) {
            Row presenceRow = table.addRow();

            Cell rowId = presenceRow.addCell();
            Cell rowEducable = presenceRow.addCell();
            Cell rowLesson = presenceRow.addCell();

            rowId.getTextView().setText(String.valueOf(presence.getId()));
            rowEducable.getTextView().setText(String.valueOf(presence.getEducable()));
            rowLesson.getTextView().setText(String.valueOf(presence.getLesson()));
        }

    }
}
