package net.bioker.android.coachorg.model;

public class Lesson {

    private int id;
    private int group;
    private String date;
    private String begin;
    private String end;
    private float price;
    private String note;

    public Lesson(int id, int group, String date, String begin, String end, float price, String note) {
        this.id = id;
        this.group = group;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.price = price;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public int getGroup() {
        return group;
    }

    public String getDate() {
        return date;
    }

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public float getPrice() {
        return price;
    }

    public String getNote() {
        return note;
    }
}
