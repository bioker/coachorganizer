package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Educable;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBEducable {

    public static final String TABLE_NAME = "educable";
    public static final String ID_FIELD_NAME = "id";
    public static final String GROUP_FIELD_NAME = "myGroup";
    public static final String NAME_FIELD_NAME = "name";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            GROUP_FIELD_NAME + " INTEGER, " +
            NAME_FIELD_NAME + " TEXT NOT NULL, " +
            "FOREIGN KEY(" + GROUP_FIELD_NAME + ") REFERENCES " +
            DBGroup.TABLE_NAME + "(" + DBGroup.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Educable educable) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_FIELD_NAME, educable.getGroup());
        contentValues.put(NAME_FIELD_NAME, educable.getName());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Educable educable) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GROUP_FIELD_NAME, educable.getGroup());
        contentValues.put(NAME_FIELD_NAME, educable.getName());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + educable.getId(), new String[]{}) > 0;
    }

    public static Educable get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int group = cursor.getInt(cursor.getColumnIndex(GROUP_FIELD_NAME));
            String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
            Educable result = new Educable(id, group, name);
            Log.d("DB", "educable found:\n" + result);
            return result;
        } else {
            Log.w("DB", "educable with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Educable> getAll(SQLiteDatabase db, String selection) {
        Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, null);
        ArrayList<Educable> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int group = cursor.getInt(cursor.getColumnIndex(GROUP_FIELD_NAME));
                String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
                Educable educable = new Educable(id, group, name);
                Log.d("DB", "educable found:\n" + result);
                result.add(educable);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find educable");
        }
        return result;
    }

    public static ArrayList<Educable> getAll(SQLiteDatabase db) {
        return getAll(db, null);
    }

    public static void fillTable(Table table, ArrayList<Educable> educables) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerGroup = header.addCell();
        Cell headerName = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerGroup.getTextView().setText(GROUP_FIELD_NAME);
        headerName.getTextView().setText(NAME_FIELD_NAME);

        for (Educable educable : educables) {
            Row educableRow = table.addRow();

            Cell cellId = educableRow.addCell();
            Cell cellGroup = educableRow.addCell();
            Cell cellName = educableRow.addCell();

            cellId.getTextView().setText(String.valueOf(educable.getId()));
            cellGroup.getTextView().setText(String.valueOf(educable.getGroup()));
            cellName.getTextView().setText(educable.getName());
        }

    }

    public static void fillTable(Table table) {
        DBConnector dbConnector = new DBConnector(table.getContext());
        fillTable(table, getAll(dbConnector.getReadableDatabase()));
    }

}
