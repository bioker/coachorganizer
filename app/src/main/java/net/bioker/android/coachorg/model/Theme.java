package net.bioker.android.coachorg.model;

public class Theme {

    private int id;
    private String name;
    private String description;
    private String manual;

    public Theme(int id, String name, String description, String manual) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.manual = manual;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getManual() {
        return manual;
    }
}
