package net.bioker.android.coachorg.model;

public class Progress {

    private int id;
    private int lesson;
    private int exercise;

    public Progress(int id, int lesson, int exercise) {
        this.id = id;
        this.lesson = lesson;
        this.exercise = exercise;
    }

    public int getId() {
        return id;
    }

    public int getLesson() {
        return lesson;
    }

    public int getExercise() {
        return exercise;
    }
}
