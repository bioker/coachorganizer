package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Exercise;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBExercise {

    public static final String TABLE_NAME = "exercise";
    public static final String ID_FIELD_NAME = "id";
    public static final String THEME_FIELD_NAME = "theme";
    public static final String NAME_FIELD_NAME = "name";
    public static final String DESCRIPTION_FIELD_NAME = "description";
    public static final String MANUAL_FIELD_NAME = "manual";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            THEME_FIELD_NAME + " INTEGER NOT NULL, " +
            NAME_FIELD_NAME + " TEXT NOT NULL, " +
            DESCRIPTION_FIELD_NAME + " TEXT, " +
            MANUAL_FIELD_NAME + " TEXT, " +
            "FOREIGN KEY(" + THEME_FIELD_NAME + ") REFERENCES " +
            DBTheme.TABLE_NAME + "(" + DBTheme.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public static boolean add(SQLiteDatabase db, Exercise exercise) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(THEME_FIELD_NAME, exercise.getTheme());
        contentValues.put(NAME_FIELD_NAME, exercise.getName());
        contentValues.put(DESCRIPTION_FIELD_NAME, exercise.getDescription());
        contentValues.put(MANUAL_FIELD_NAME, exercise.getManual());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Exercise exercise) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(THEME_FIELD_NAME, exercise.getTheme());
        contentValues.put(NAME_FIELD_NAME, exercise.getName());
        contentValues.put(DESCRIPTION_FIELD_NAME, exercise.getDescription());
        contentValues.put(MANUAL_FIELD_NAME, exercise.getManual());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + exercise.getId(), new String[]{}) > 0;
    }

    public static Exercise get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int theme = cursor.getInt(cursor.getColumnIndex(THEME_FIELD_NAME));
            String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
            String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION_FIELD_NAME));
            String manual = cursor.getString(cursor.getColumnIndex(MANUAL_FIELD_NAME));
            Exercise result = new Exercise(id, theme, name, description, manual);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Exercise> getAll(SQLiteDatabase db) {
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<Exercise> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int theme = cursor.getInt(cursor.getColumnIndex(THEME_FIELD_NAME));
                String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
                String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION_FIELD_NAME));
                String manual = cursor.getString(cursor.getColumnIndex(MANUAL_FIELD_NAME));
                Exercise exercise = new Exercise(id, theme, name, description, manual);
                Log.d("DB", "exercise found:\n" + result);
                result.add(exercise);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerTheme = header.addCell();
        Cell headerName = header.addCell();
        Cell headerDescription = header.addCell();
        Cell headerManual = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerTheme.getTextView().setText(THEME_FIELD_NAME);
        headerName.getTextView().setText(NAME_FIELD_NAME);
        headerDescription.getTextView().setText(DESCRIPTION_FIELD_NAME);
        headerManual.getTextView().setText(MANUAL_FIELD_NAME);

        ArrayList<Exercise> exercises = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Exercise exercise : exercises) {
            Row exerciseRow = table.addRow();

            Cell rowId = exerciseRow.addCell();
            Cell rowTheme = exerciseRow.addCell();
            Cell rowName = exerciseRow.addCell();
            Cell rowDescription = exerciseRow.addCell();
            Cell rowManual = exerciseRow.addCell();

            rowId.getTextView().setText(String.valueOf(exercise.getId()));
            rowTheme.getTextView().setText(String.valueOf(exercise.getTheme()));
            rowName.getTextView().setText(exercise.getName());
            rowDescription.getTextView().setText(exercise.getDescription());
            rowManual.getTextView().setText(exercise.getManual());
        }

    }
}
