package net.bioker.android.coachorg.model;

public class Presence {

    private int id;
    private int educable;
    private int lesson;

    public Presence(int id, int educable, int lesson) {
        this.id = id;
        this.educable = educable;
        this.lesson = lesson;
    }

    public int getId() {
        return id;
    }

    public int getEducable() {
        return educable;
    }

    public int getLesson() {
        return lesson;
    }
}
