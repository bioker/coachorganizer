package net.bioker.android.coachorg.fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import net.bioker.android.coachorg.R;
import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.db.model.DBEducable;
import net.bioker.android.coachorg.db.model.DBEducableInfo;
import net.bioker.android.coachorg.db.model.DBExercise;
import net.bioker.android.coachorg.db.model.DBGroup;
import net.bioker.android.coachorg.db.model.DBLesson;
import net.bioker.android.coachorg.db.model.DBPayment;
import net.bioker.android.coachorg.db.model.DBPresence;
import net.bioker.android.coachorg.db.model.DBProgress;
import net.bioker.android.coachorg.db.model.DBTheme;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DebugFragment extends Fragment {

    private FrameLayout tableContainer;
    private Spinner tableSelect;

    private String[] tableNames;
    private Table table;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_debug, null);
        tableContainer = (FrameLayout) v.findViewById(R.id.debugTableContainer);
        table = new Table(v.getContext());
        table.addTable(tableContainer);
        if (getActivity() == null) Log.w("DEBUG", "activity for debug fragment is null!");

        final DBConnector dbConnector = new DBConnector(v.getContext());

        tableSelect = (Spinner) v.findViewById(R.id.debugTableSelect);
        tableNames = getTableNames(dbConnector.getWritableDatabase());

        ArrayAdapter<String> tableSelectAdapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_spinner_item,
                tableNames);
        tableSelectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tableSelect.setAdapter(tableSelectAdapter);
        tableSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("DEBUG", "selected table - " + tableNames[position]);
                switch (tableNames[position]) {
                    case DBEducable.TABLE_NAME: {
                        table.clear();
                        DBEducable.fillTable(table, DBEducable.getAll(dbConnector.getReadableDatabase()));
                    }
                    break;
                    case DBEducableInfo.TABLE_NAME: {
                        table.clear();
                        DBEducableInfo.fillTable(table);
                    }
                    break;
                    case DBExercise.TABLE_NAME: {
                        table.clear();
                        DBExercise.fillTable(table);
                    }
                    break;
                    case DBGroup.TABLE_NAME: {
                        table.clear();
                        DBGroup.fillTable(table);
                    }
                    break;
                    case DBLesson.TABLE_NAME: {
                        table.clear();
                        DBLesson.fillTable(table);
                    }
                    break;
                    case DBPayment.TABLE_NAME: {
                        table.clear();
                        DBPayment.fillTable(table);
                    }
                    break;
                    case DBPresence.TABLE_NAME: {
                        table.clear();
                        DBPresence.fillTable(table);
                    }
                    break;
                    case DBProgress.TABLE_NAME: {
                        table.clear();
                        DBProgress.fillTable(table);
                    }
                    break;
                    case DBTheme.TABLE_NAME: {
                        table.clear();
                        DBTheme.fillTable(table);
                    }
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return v;
    }

    private String[] getTableNames(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        ArrayList<String> preResult = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Log.d("DB", "found table - " + c.getString(0));
                preResult.add(c.getString(0));
            } while (c.moveToNext());
        }
        String[] result = new String[preResult.size()];
        preResult.toArray(result);
        return result;
    }
}
