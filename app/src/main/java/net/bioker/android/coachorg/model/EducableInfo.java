package net.bioker.android.coachorg.model;

public class EducableInfo {

    private int id;
    private int educable;
    private String name;
    private String info;

    public EducableInfo(int id, int educable, String name, String value) {
        this.id = id;
        this.educable = educable;
        this.name = name;
        this.info = value;
    }

    public int getId() {
        return id;
    }

    public int getEducable() {
        return educable;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }
}
