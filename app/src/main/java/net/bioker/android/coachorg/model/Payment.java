package net.bioker.android.coachorg.model;

public class Payment {

    private int id;
    private int educable;
    private String date;
    private float payment;

    public Payment(int id, int educable, String date, float payment) {
        this.id = id;
        this.educable = educable;
        this.date = date;
        this.payment = payment;
    }

    public int getId() {
        return id;
    }

    public int getEducable() {
        return educable;
    }

    public String getDate() {
        return date;
    }

    public float getPayment() {
        return payment;
    }
}
