package net.bioker.android.coachorg.model;

public class Exercise {

    private int id;
    private int theme;
    private String name;
    private String description;
    private String manual;

    public Exercise(int id, int theme, String name, String description, String manual) {
        this.id = id;
        this.theme = theme;
        this.name = name;
        this.description = description;
        this.manual = manual;
    }

    public int getId() {
        return id;
    }

    public int getTheme() {
        return theme;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getManual() {
        return manual;
    }
}
