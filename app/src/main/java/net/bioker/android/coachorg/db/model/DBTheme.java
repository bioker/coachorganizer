package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Theme;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBTheme {

    public static final String TABLE_NAME = "theme";
    public static final String ID_FIELD_NAME = "id";
    public static final String NAME_FIELD_NAME = "name";
    public static final String DESCRIPTION_FIELD_NAME = "description";
    public static final String MANUAL_FIELD_NAME = "manual";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            NAME_FIELD_NAME + " TEXT NOT NULL, " +
            DESCRIPTION_FIELD_NAME + " TEXT, " +
            MANUAL_FIELD_NAME + " TEXT);";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Theme theme) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_FIELD_NAME, theme.getName());
        contentValues.put(DESCRIPTION_FIELD_NAME, theme.getDescription());
        contentValues.put(MANUAL_FIELD_NAME, theme.getManual());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Theme theme) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_FIELD_NAME, theme.getName());
        contentValues.put(DESCRIPTION_FIELD_NAME, theme.getDescription());
        contentValues.put(MANUAL_FIELD_NAME, theme.getManual());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + theme.getId(), new String[]{}) > 0;
    }

    public static Theme get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
            String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION_FIELD_NAME));
            String manual = cursor.getString(cursor.getColumnIndex(MANUAL_FIELD_NAME));
            Theme result = new Theme(id, name, description, manual);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Theme> getAll(SQLiteDatabase db) {
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<Theme> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
                String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION_FIELD_NAME));
                String manual = cursor.getString(cursor.getColumnIndex(MANUAL_FIELD_NAME));
                Theme theme = new Theme(id, name, description, manual);
                Log.d("DB", "exercise found:\n" + result);
                result.add(theme);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerName = header.addCell();
        Cell headerDescription = header.addCell();
        Cell headerManual = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerName.getTextView().setText(NAME_FIELD_NAME);
        headerDescription.getTextView().setText(DESCRIPTION_FIELD_NAME);
        headerManual.getTextView().setText(MANUAL_FIELD_NAME);

        ArrayList<Theme> themes = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Theme theme : themes) {
            Row themeRow = table.addRow();

            Cell rowId = themeRow.addCell();
            Cell rowName = themeRow.addCell();
            Cell rowDescription = themeRow.addCell();
            Cell rowManual = themeRow.addCell();

            rowId.getTextView().setText(String.valueOf(theme.getId()));
            rowName.getTextView().setText(theme.getName());
            rowDescription.getTextView().setText(theme.getDescription());
            rowManual.getTextView().setText(theme.getManual());
        }

    }
}
