package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.EducableInfo;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;
import java.util.HashMap;

public class DBEducableInfo {

    public static final String TABLE_NAME = "educableInfo";
    public static final String ID_FIELD_NAME = "id";
    public static final String EDUCABLE_FIELD_NAME = "educable";
    public static final String NAME_FIELD_NAME = "name";
    public static final String INFO_FIELD_NAME = "info";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            EDUCABLE_FIELD_NAME + " INTEGER NOT NULL, " +
            NAME_FIELD_NAME + " TEXT NOT NULL, " +
            INFO_FIELD_NAME + " TEXT NOT NULL, " +
            "FOREIGN KEY(" + EDUCABLE_FIELD_NAME + ") REFERENCES " +
            DBEducable.TABLE_NAME + "(" + DBEducable.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public static boolean add(SQLiteDatabase db, EducableInfo educableInfo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, educableInfo.getEducable());
        contentValues.put(NAME_FIELD_NAME, educableInfo.getName());
        contentValues.put(INFO_FIELD_NAME, educableInfo.getInfo());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, EducableInfo educableInfo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(EDUCABLE_FIELD_NAME, educableInfo.getEducable());
        contentValues.put(NAME_FIELD_NAME, educableInfo.getName());
        contentValues.put(INFO_FIELD_NAME, educableInfo.getInfo());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + educableInfo.getId(), new String[]{}) > 0;
    }

    public static EducableInfo get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
            String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
            String info = cursor.getString(cursor.getColumnIndex(INFO_FIELD_NAME));
            EducableInfo result = new EducableInfo(id, educable, name, info);
            Log.d("DB", "educable info found:\n" + result);
            return result;
        } else {
            Log.w("DB", "educable info with " + id + " not found");
        }
        return null;
    }

    public static String getEducableInfo(SQLiteDatabase db, int educableId, String infoName) {
        HashMap<String, String> condition = new HashMap<>();
        condition.put(DBEducableInfo.EDUCABLE_FIELD_NAME, String.valueOf(educableId));
        condition.put(DBEducableInfo.NAME_FIELD_NAME, "'" + infoName + "'");
        ArrayList<EducableInfo> result = DBEducableInfo.getAll(
                db,
                DBConnector.getSelection(condition)
        );
        if (result.size() > 1) {
            Log.w("DB", "educable info more than 1 for educable and infoName pare");
        }
        if (result.size() > 0) {
            return result.get(0).getInfo();
        } else {
            return "";
        }
    }

    public static ArrayList<EducableInfo> getAll(SQLiteDatabase db, String selection) {
        Cursor cursor = db.query(TABLE_NAME, null, selection, null, null, null, null);
        ArrayList<EducableInfo> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int educable = cursor.getInt(cursor.getColumnIndex(EDUCABLE_FIELD_NAME));
                String name = cursor.getString(cursor.getColumnIndex(NAME_FIELD_NAME));
                String info = cursor.getString(cursor.getColumnIndex(INFO_FIELD_NAME));
                EducableInfo educableInfo = new EducableInfo(id, educable, name, info);
                Log.d("DB", "educable info found:\n" + result);
                result.add(educableInfo);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find educable info");
        }
        return result;
    }

    public static ArrayList<EducableInfo> getAll(SQLiteDatabase db) {
        return getAll(db, null);
    }

    public static ArrayList<String> getDistinctNames(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT DISTINCT " + NAME_FIELD_NAME + " FROM " + TABLE_NAME, null);
        ArrayList<String> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                Log.d("DB", "found educable info name - " + c.getString(0));
                result.add(c.getString(0));
            } while (c.moveToNext());
        } else {
            Log.d("DB", "educable info names not found");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerEducable = header.addCell();
        Cell headerName = header.addCell();
        Cell headerInfo = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerEducable.getTextView().setText(EDUCABLE_FIELD_NAME);
        headerName.getTextView().setText(NAME_FIELD_NAME);
        headerInfo.getTextView().setText(INFO_FIELD_NAME);

        ArrayList<EducableInfo> educableInfos = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (EducableInfo educableInfo : educableInfos) {
            Row educableInfoRow = table.addRow();

            Cell cellId = educableInfoRow.addCell();
            Cell cellGroup = educableInfoRow.addCell();
            Cell cellName = educableInfoRow.addCell();
            Cell cellInfo = educableInfoRow.addCell();

            cellId.getTextView().setText(String.valueOf(educableInfo.getId()));
            cellGroup.getTextView().setText(String.valueOf(educableInfo.getEducable()));
            cellName.getTextView().setText(educableInfo.getName());
            cellInfo.getTextView().setText(educableInfo.getInfo());
        }

    }
}
