package net.bioker.android.coachorg.table;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TableLayout;

import java.util.ArrayList;

public class Table {

    private ScrollView verticalScrollView;
    private HorizontalScrollView horizontalScrollView;

    private Context context;
    private TableLayout tableLayout;
    private ArrayList<Row> rows;

    public Table(Context context) {

        verticalScrollView = new ScrollView(context);

        horizontalScrollView = new HorizontalScrollView(context);
        horizontalScrollView.setVerticalScrollBarEnabled(true);
        horizontalScrollView.setHorizontalScrollBarEnabled(true);

        this.context = context;
        tableLayout = new TableLayout(this.context);
        tableLayout.setStretchAllColumns(false);
        tableLayout.setShrinkAllColumns(false);

        rows = new ArrayList<>();

        horizontalScrollView.addView(tableLayout);
        verticalScrollView.addView(horizontalScrollView);
    }

    public Row addRow() {
        Row row = new Row(rows.size(), this);
        rows.add(row);
        tableLayout.addView(row.getTableRow());
        return row;
    }

    public Row getRow(int i) {
        return rows.get(i);
    }

    public void removeRow(int i) {
        tableLayout.removeView(rows.get(i).getTableRow());
        rows.remove(i);
    }

    public Context getContext() {
        return context;
    }

    public void addTable(ViewGroup viewGroup) {
        viewGroup.addView(verticalScrollView);
    }

    public TableLayout getTableLayout() {
        return tableLayout;
    }

    public void clear() {
        for (int i = 0; i < rows.size(); i++) {
            rows.get(i).clear();
            tableLayout.removeView(rows.get(i).getTableRow());
        }
        rows.clear();
    }
}
