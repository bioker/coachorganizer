package net.bioker.android.coachorg.fragments.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.bioker.android.coachorg.R;

public class EducableDialogFragment extends DialogFragment {

    private static final String TAG = EducableDialogFragment.class.getSimpleName();

    private TextView dialogText;
    private Button dialogButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_educable, null);
        dialogText = (TextView) v.findViewById(R.id.educableDialogText);
        dialogText.setText("dialog text");
        dialogButton = (Button) v.findViewById(R.id.educableDialogButton);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "dialog click");
            }
        });
        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(TAG, "dialog dismiss");
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(TAG, "dialog cancel");
    }
}
