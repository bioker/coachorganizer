package net.bioker.android.coachorg.table;

import android.content.Context;
import android.graphics.Color;
import android.widget.TableRow;

import java.util.ArrayList;

public class Row {

    private int number;
    private Context context;
    private TableRow tableRow;
    private Table origin;
    private ArrayList<Cell> cells;

    public Row(int number, Table origin) {
        this.number = number;
        this.origin = origin;
        this.context = origin.getContext();
        tableRow = new TableRow(this.context);
        tableRow.setBackgroundColor(Color.BLACK);
        tableRow.setPadding(0, 5, 0, 5);
        cells = new ArrayList<>();
    }

    public Cell addCell() {
        Cell cell = new Cell(cells.size(), this);
        cells.add(cell);
        tableRow.addView(cell.getTextView());
        return cell;
    }

    public Cell getCell(int i) {
        return cells.get(i);
    }

    public void removeCell(int i) {
        tableRow.removeView(cells.get(i).getTextView());
        cells.remove(i);
    }

    public Context getContext() {
        return context;
    }

    public int getNumber() {
        return number;
    }

    public TableRow getTableRow() {
        return tableRow;
    }

    public Table getOrigin() {
        return origin;
    }

    public ArrayList<Cell> getCells() {
        return cells;
    }

    public void clear() {
        for (int i = 0; i < cells.size(); i++) {
            tableRow.removeView(cells.get(i).getTextView());
        }
        cells.clear();
    }
}
