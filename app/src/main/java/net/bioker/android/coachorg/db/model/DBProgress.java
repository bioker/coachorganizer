package net.bioker.android.coachorg.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.model.Progress;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class DBProgress {

    public static final String TABLE_NAME = "progress";
    public static final String ID_FIELD_NAME = "id";
    public static final String LESSON_FIELD_NAME = "lesson";
    public static final String EXERCISE_FIELD_NAME = "exercise";

    public static String createSQL = "CREATE TABLE " + TABLE_NAME + "(" +
            ID_FIELD_NAME + " INTEGER PRIMARY KEY, " +
            LESSON_FIELD_NAME + " INTEGER NOT NULL, " +
            EXERCISE_FIELD_NAME + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + EXERCISE_FIELD_NAME + ") REFERENCES " +
            DBExercise.TABLE_NAME + "(" + DBExercise.ID_FIELD_NAME + ")," +
            "FOREIGN KEY(" + LESSON_FIELD_NAME + ") REFERENCES " +
            DBLesson.TABLE_NAME + "(" + DBLesson.ID_FIELD_NAME + "));";
    public static String dropSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean add(SQLiteDatabase db, Progress progress) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(LESSON_FIELD_NAME, progress.getLesson());
        contentValues.put(EXERCISE_FIELD_NAME, progress.getExercise());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public static boolean remove(SQLiteDatabase db, int id) {
        return db.delete(TABLE_NAME, "WHERE " + ID_FIELD_NAME + "=" + id, new String[]{}) > 0;
    }

    public static boolean update(SQLiteDatabase db, Progress progress) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(LESSON_FIELD_NAME, progress.getLesson());
        contentValues.put(EXERCISE_FIELD_NAME, progress.getExercise());
        return db.update(TABLE_NAME, contentValues,
                "WHERE " + ID_FIELD_NAME + "=" + progress.getId(), new String[]{}) > 0;
    }

    public static Progress get(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(TABLE_NAME, null, "WHERE " + ID_FIELD_NAME + "=" + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            int lesson = cursor.getInt(cursor.getColumnIndex(LESSON_FIELD_NAME));
            int exercise = cursor.getInt(cursor.getColumnIndex(EXERCISE_FIELD_NAME));
            Progress result = new Progress(id, lesson, exercise);
            Log.d("DB", "exercise found:\n" + result);
            return result;
        } else {
            Log.w("DB", "exercise with " + id + " not found");
        }
        return null;
    }

    public static ArrayList<Progress> getAll(SQLiteDatabase db) {
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<Progress> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD_NAME));
                int lesson = cursor.getInt(cursor.getColumnIndex(LESSON_FIELD_NAME));
                int exercise = cursor.getInt(cursor.getColumnIndex(EXERCISE_FIELD_NAME));
                Progress progress = new Progress(id, lesson, exercise);
                Log.d("DB", "exercise found:\n" + result);
                result.add(progress);
            } while (cursor.moveToNext());
        } else {
            Log.w("DB", "can't find exercise");
        }
        return result;
    }

    public static void fillTable(Table table) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerId = header.addCell();
        Cell headerLesson = header.addCell();
        Cell headerExercise = header.addCell();

        headerId.getTextView().setText(ID_FIELD_NAME);
        headerLesson.getTextView().setText(LESSON_FIELD_NAME);
        headerExercise.getTextView().setText(EXERCISE_FIELD_NAME);

        ArrayList<Progress> progresses = getAll(
                new DBConnector(table.getContext()).getWritableDatabase());

        for (Progress progress : progresses) {
            Row progressRow = table.addRow();

            Cell rowId = progressRow.addCell();
            Cell rowLesson = progressRow.addCell();
            Cell rowExercise = progressRow.addCell();

            rowId.getTextView().setText(String.valueOf(progress.getId()));
            rowLesson.getTextView().setText(String.valueOf(progress.getLesson()));
            rowExercise.getTextView().setText(String.valueOf(progress.getExercise()));
        }

    }
}
