package net.bioker.android.coachorg.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.bioker.android.coachorg.R;
import net.bioker.android.coachorg.db.model.DBEducable;
import net.bioker.android.coachorg.db.model.DBEducableInfo;
import net.bioker.android.coachorg.db.model.DBExercise;
import net.bioker.android.coachorg.db.model.DBGroup;
import net.bioker.android.coachorg.db.model.DBLesson;
import net.bioker.android.coachorg.db.model.DBPayment;
import net.bioker.android.coachorg.db.model.DBPresence;
import net.bioker.android.coachorg.db.model.DBProgress;
import net.bioker.android.coachorg.db.model.DBTheme;
import net.bioker.android.coachorg.model.Educable;
import net.bioker.android.coachorg.model.EducableInfo;
import net.bioker.android.coachorg.model.Exercise;
import net.bioker.android.coachorg.model.Group;
import net.bioker.android.coachorg.model.Lesson;
import net.bioker.android.coachorg.model.Payment;
import net.bioker.android.coachorg.model.Presence;
import net.bioker.android.coachorg.model.Progress;
import net.bioker.android.coachorg.model.Theme;
import net.bioker.android.coachorg.table.Cell;
import net.bioker.android.coachorg.table.Row;
import net.bioker.android.coachorg.table.Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class DBConnector extends SQLiteOpenHelper {

    private static final String NAME = "teach_them_db";
    private static final int VERSION = 1;

    public DBConnector(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DBGroup.dropSQL);
        db.execSQL(DBEducable.dropSQL);
        db.execSQL(DBEducableInfo.dropSQL);
        db.execSQL(DBPayment.dropSQL);
        db.execSQL(DBLesson.dropSQL);
        db.execSQL(DBPresence.dropSQL);
        db.execSQL(DBTheme.dropSQL);
        db.execSQL(DBExercise.dropSQL);
        db.execSQL(DBProgress.dropSQL);

        db.execSQL(DBGroup.createSQL);
        db.execSQL(DBEducable.createSQL);
        db.execSQL(DBEducableInfo.createSQL);
        db.execSQL(DBPayment.createSQL);
        db.execSQL(DBLesson.createSQL);
        db.execSQL(DBPresence.createSQL);
        db.execSQL(DBTheme.createSQL);
        db.execSQL(DBExercise.createSQL);
        db.execSQL(DBProgress.createSQL);

        testInitialize(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void testInitialize(SQLiteDatabase db) {
        DBGroup.add(db, new Group(0, "group1"));
        DBGroup.add(db, new Group(0, "group2"));
        DBGroup.add(db, new Group(0, "group3"));

        DBEducable.add(db, new Educable(0, 1, "educable1"));
        DBEducable.add(db, new Educable(0, 1, "educable2"));
        DBEducable.add(db, new Educable(0, 1, "educable3"));
        DBEducable.add(db, new Educable(0, 2, "educable4"));
        DBEducable.add(db, new Educable(0, 2, "educable5"));
        DBEducable.add(db, new Educable(0, 3, "educable6"));

        DBEducableInfo.add(db, new EducableInfo(0, 1, "phone", "educable1phone"));
        DBEducableInfo.add(db, new EducableInfo(0, 2, "phone", "educable2phone"));
        DBEducableInfo.add(db, new EducableInfo(0, 3, "phone", "educable3phone"));
        DBEducableInfo.add(db, new EducableInfo(0, 4, "phone", "educable4phone"));
        DBEducableInfo.add(db, new EducableInfo(0, 5, "phone", "educable5phone"));
        DBEducableInfo.add(db, new EducableInfo(0, 6, "phone", "educable6phone"));

        DBPayment.add(db, new Payment(0, 1, "2016-08-05", 10000));
        DBPayment.add(db, new Payment(0, 2, "2016-08-05", 10000));
        DBPayment.add(db, new Payment(0, 3, "2016-08-05", 10000));

        DBLesson.add(db, new Lesson(0, 1, "2016-09-01", "17:00:00", "17:45:00", 1000, "note"));
        DBLesson.add(db, new Lesson(0, 2, "2016-09-01", "18:00:00", "18:45:00", 1000, "note"));
        DBLesson.add(db, new Lesson(0, 3, "2016-09-01", "19:00:00", "19:45:00", 1000, "note"));

        DBPresence.add(db, new Presence(0, 1, 1));
        DBPresence.add(db, new Presence(0, 3, 1));
        DBPresence.add(db, new Presence(0, 4, 2));
        DBPresence.add(db, new Presence(0, 6, 3));

        DBTheme.add(db, new Theme(0, "theme1", "theme1description", "theme1manual"));
        DBTheme.add(db, new Theme(0, "theme2", "theme2description", "theme2manual"));
        DBTheme.add(db, new Theme(0, "theme3", "theme3description", "theme3manual"));

        DBExercise.add(db, new Exercise(0, 1, "exercise1", "exercise1description", "exercise1manual"));
        DBExercise.add(db, new Exercise(0, 1, "exercise2", "exercise2description", "exercise2manual"));
        DBExercise.add(db, new Exercise(0, 1, "exercise3", "exercise3description", "exercise3manual"));
        DBExercise.add(db, new Exercise(0, 2, "exercise4", "exercise4description", "exercise4manual"));
        DBExercise.add(db, new Exercise(0, 2, "exercise5", "exercise5description", "exercise5manual"));
        DBExercise.add(db, new Exercise(0, 2, "exercise6", "exercise6description", "exercise6manual"));
        DBExercise.add(db, new Exercise(0, 3, "exercise7", "exercise7description", "exercise7manual"));
        DBExercise.add(db, new Exercise(0, 3, "exercise8", "exercise8description", "exercise8manual"));
        DBExercise.add(db, new Exercise(0, 3, "exercise9", "exercise9description", "exercise9manual"));

        DBProgress.add(db, new Progress(0, 1, 1));
        DBProgress.add(db, new Progress(0, 1, 2));
        DBProgress.add(db, new Progress(0, 1, 3));
        DBProgress.add(db, new Progress(0, 2, 1));
        DBProgress.add(db, new Progress(0, 2, 2));
        DBProgress.add(db, new Progress(0, 2, 3));
    }

    public static String getSelection(String field, String value) {
        return field + "=" + value;
    }

    public static String getSelection(HashMap<String, String> condition) {
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (String field : condition.keySet()) {
            result.append(field);
            result.append("=");
            result.append(condition.get(field));
            if (++count < condition.size())
                result.append(" AND ");
        }
        Log.d("SQL", "condition - " + result);
        return result.toString();
    }

    public static void fillEducableTable(SQLiteDatabase db, Table table, ArrayList<Educable> educables) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerEducable = header.addCell();

        headerEducable.getTextView().setText(DBEducable.TABLE_NAME);

        ArrayList<String> infoHeaders = DBEducableInfo.getDistinctNames(db);
        ArrayList<Cell> headerInfo = new ArrayList<>();

        for (String infoHeader : infoHeaders) {
            Cell cell = header.addCell();
            cell.getTextView().setText(infoHeader);
            headerInfo.add(cell);
        }

        for (Educable educable : educables) {
            Row row = table.addRow();
            Cell educableCell = row.addCell();
            educableCell.getTextView().setText(educable.getName());
            for (String infoHeader : infoHeaders) {
                Cell cell = row.addCell();
                cell.getTextView().setText(
                        DBEducableInfo.getEducableInfo(
                                db,
                                educable.getId(),
                                infoHeader
                        )
                );
            }
        }
    }

    public static void fillPaymentTable(SQLiteDatabase db, Table table, ArrayList<Educable> educables) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerEducable = header.addCell();

        headerEducable.getTextView().setText(DBEducable.TABLE_NAME);

        String[] months = table.getContext().getResources().getStringArray(R.array.months);

        for (String month : months) {
            Cell cell = header.addCell();
            cell.getTextView().setText(month);
        }

        for (Educable educable : educables) {
            Row row = table.addRow();

            Cell educableCell = row.addCell();
            educableCell.getTextView().setText(educable.getName());

            int number = 1;
            for (String month : months) {
                Cell cell = row.addCell();
                cell.getTextView().setText(DBPayment.getEducablePayment(
                        db,
                        educable,
                        number++
                ));
            }
        }
    }

    public static void fillPresenceTable(SQLiteDatabase db, Table table, Group group) {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        Cell headerEducable = header.addCell();

        headerEducable.getTextView().setText(DBEducable.TABLE_NAME);

        ArrayList<Lesson> lessons = DBLesson.getLessonByGroup(db, group);
        ArrayList<Educable> educables = DBEducable.getAll(
                db, getSelection(DBEducable.GROUP_FIELD_NAME, String.valueOf(group.getId())));

        for (Lesson lesson : lessons) {
            Cell cell = header.addCell();
            cell.getTextView().setText(lesson.getDate());
        }

        for (Educable educable : educables) {
            Row row = table.addRow();

            Cell educableCell = row.addCell();
            educableCell.getTextView().setText(educable.getName());

            for (Lesson lesson : lessons) {
                HashMap<String, String> selectionMap = new HashMap<>();
                selectionMap.put(DBPresence.LESSON_FIELD_NAME, String.valueOf(lesson.getId()));
                selectionMap.put(DBPresence.EDUCABLE_FIELD_NAME, String.valueOf(educable.getId()));
                String selecttion = getSelection(selectionMap);
                ArrayList<Presence> presences = DBPresence.getAll(db, selecttion);
                String value;
                if (presences.size() > 0) {
                    value = "+";
                } else {
                    value = "-";
                }
                Cell cell = row.addCell();
                cell.getTextView().setText(value);
            }
        }
    }

    public static void fillScheduleTable(SQLiteDatabase db, Table table, String date) throws ParseException {
        Row header = table.addRow();
        header.getTableRow().setPadding(0, 10, 0, 10);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
        SimpleDateFormat sqliteDate = new SimpleDateFormat("yyyy-MM-dd");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(date));
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONTH);

        Cell headerTime = header.addCell();

        headerTime.getTextView().setText("time");

        Date beginDate = null;
        Date endDate = null;

        ArrayList<String> dates = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            Cell day = header.addCell();
            day.getTextView().setText(sqliteDate.format(calendar.getTime()));
            dates.add(sqliteDate.format(calendar.getTime()));
            if (i == 0) beginDate = calendar.getTime();
            if (i == 6) endDate = calendar.getTime();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        ArrayList<String> beginTimes = DBLesson.getDistinctBeginTimeByDate(
                db, sqliteDate.format(beginDate), sqliteDate.format(endDate));
        for (String time : beginTimes) {
            Row row = table.addRow();
            Cell rowHeader = row.addCell();
            rowHeader.getTextView().setText(time);

            String lastTime = "23:59:59";

            for (String lessonDate : dates) {
                String endTime;
                if (beginTimes.indexOf(time) < (beginTimes.size() - 1))
                    endTime = beginTimes.get(beginTimes.indexOf(time) + 1);
                else
                    endTime = lastTime;
                ArrayList<Group> groups = DBLesson.getGroupsByDateAndTime(db, lessonDate, time, endTime);
                StringBuilder cellText = new StringBuilder();
                for (Group group : groups) {
                    cellText.append(group.getName()).append("\n");
                }
                Cell cell = row.addCell();
                cell.getTextView().setText(cellText.toString());
            }

        }
    }
}
