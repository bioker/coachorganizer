package net.bioker.android.coachorg.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import net.bioker.android.coachorg.R;
import net.bioker.android.coachorg.db.DBConnector;
import net.bioker.android.coachorg.db.model.DBEducable;
import net.bioker.android.coachorg.db.model.DBGroup;
import net.bioker.android.coachorg.fragments.dialogs.EducableDialogFragment;
import net.bioker.android.coachorg.model.Educable;
import net.bioker.android.coachorg.model.Group;
import net.bioker.android.coachorg.table.Table;

import java.util.ArrayList;

public class EducableFragment extends Fragment {

    private DBConnector dbConnector;
    private Spinner groupSelect;
    private FrameLayout tableContainer;
    private ArrayList<Group> groups;
    private Table table;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_educable, null);

        dbConnector = new DBConnector(v.getContext());

        groupSelect = (Spinner) v.findViewById(R.id.educableGroupSelect);
        tableContainer = (FrameLayout) v.findViewById(R.id.educableTableContainer);

        table = new Table(v.getContext());
        table.addTable(tableContainer);

        groups = DBGroup.getAll(dbConnector.getReadableDatabase());

        setupGroupSpinner();

        return v;
    }

    private void setupGroupSpinner() {
        ArrayList<String> groupNames = new ArrayList<>();
        for (Group group : groups) {
            groupNames.add(group.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                groupSelect.getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                groupNames);
        groupSelect.setAdapter(adapter);
        groupSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateTable(groups.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateTable(Group group) {
        table.clear();
        ArrayList<Educable> educables = DBEducable.getAll(
                dbConnector.getReadableDatabase(),
                DBConnector.getSelection(
                        DBEducable.GROUP_FIELD_NAME,
                        String.valueOf(group.getId()))
        );
        DBConnector.fillEducableTable(
                dbConnector.getReadableDatabase(),
                table,
                educables);
        table.getTableLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new EducableDialogFragment();
                dialogFragment.show(getFragmentManager(), "educableDialog");
            }
        });
    }
}
