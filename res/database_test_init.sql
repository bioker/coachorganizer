INSERT INTO myGroup VALUES(null,'group1');
INSERT INTO myGroup VALUES(null,'group2');
INSERT INTO myGroup VALUES(null,'group3');

INSERT INTO educable VALUES(null,1, 'educable1-name1');
INSERT INTO educable VALUES(null,1, 'educable1-name2');
INSERT INTO educable VALUES(null,1, 'educable1-name3');
INSERT INTO educable VALUES(null,2, 'educable1-name4');
INSERT INTO educable VALUES(null,2. 'educable1-name5');
INSERT INTO educable VALUES(null,2, 'educable1-name6');
INSERT INTO educable VALUES(null,3, 'educable1-name7');
INSERT INTO educable VALUES(null,3, 'educable1-name8');
INSERT INTO educable VALUES(null,3, 'educable1-name9');

INSERT INTO educableInfo VALUES(null,1,'name','educable1-name');
INSERT INTO educableInfo VALUES(null,1,'phone','educable1-phone');
INSERT INTO educableInfo VALUES(null,2,'name','educable2-name');
INSERT INTO educableInfo VALUES(null,2,'phone','educable2-phone');
INSERT INTO educableInfo VALUES(null,3,'name','educable3-name');
INSERT INTO educableInfo VALUES(null,3,'phone','educable3-phone');
INSERT INTO educableInfo VALUES(null,4,'name','educable4-name');
INSERT INTO educableInfo VALUES(null,4,'phone','educable4-phone');
INSERT INTO educableInfo VALUES(null,5,'name','educable5-name');
INSERT INTO educableInfo VALUES(null,5,'phone','educable5-phone');
INSERT INTO educableInfo VALUES(null,6,'name','educable6-name');
INSERT INTO educableInfo VALUES(null,6,'phone','educable6-phone');
INSERT INTO educableInfo VALUES(null,7,'name','educable7-name');
INSERT INTO educableInfo VALUES(null,7,'phone','educable7-phone');
INSERT INTO educableInfo VALUES(null,8,'name','educable8-name');
INSERT INTO educableInfo VALUES(null,8,'phone','educable8-phone');
INSERT INTO educableInfo VALUES(null,9,'name','educable9-name');
INSERT INTO educableInfo VALUES(null,9,'phone','educable9-phone');

INSERT INTO payment VALUES(null,1,'2014-02-02',1000);
INSERT INTO payment VALUES(null,2,'2014-02-02',1000);
INSERT INTO payment VALUES(null,3,'2014-02-02',1000);
INSERT INTO payment VALUES(null,4,'2014-02-02',1000);
INSERT INTO payment VALUES(null,5,'2014-02-02',1000);
INSERT INTO payment VALUES(null,6,'2014-02-02',1000);
INSERT INTO payment VALUES(null,7,'2014-02-02',1000);
INSERT INTO payment VALUES(null,8,'2014-02-02',1000);
INSERT INTO payment VALUES(null,9,'2014-02-02',1000);

INSERT INTO lesson VALUES(null,1,'2016-01-01','16:00:00','16:45:00',300,'lesson1-note');
INSERT INTO lesson VALUES(null,2,'2016-02-01','17:00:00','17:45:00',300,'lesson2-note');
INSERT INTO lesson VALUES(null,3,'2016-03-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,1,'2016-04-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,2,'2016-05-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,3,'2016-06-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,1,'2016-07-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,2,'2016-08-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,3,'2016-09-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,1,'2016-10-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,2,'2016-11-01','18:00:00','18:45:00',300,'lesson3-note');
INSERT INTO lesson VALUES(null,3,'2016-12-01','18:00:00','18:45:00',300,'lesson3-note');

INSERT INTO presence VALUES(null,1,1);
INSERT INTO presence VALUES(null,2,1);
INSERT INTO presence VALUES(null,3,1);
INSERT INTO presence VALUES(null,4,2);
INSERT INTO presence VALUES(null,5,2);
INSERT INTO presence VALUES(null,6,2);
INSERT INTO presence VALUES(null,7,3);
INSERT INTO presence VALUES(null,8,3);
INSERT INTO presence VALUES(null,9,3);

INSERT INTO theme VALUES(null,'theme1', 'theme1-description', 'theme1-manual');
INSERT INTO theme VALUES(null,'theme2', 'theme2-description', 'theme2-manual');
INSERT INTO theme VALUES(null,'theme3', 'theme3-description', 'theme3-manual');

INSERT INTO exercise VALUES(null,1, 'exercise1', 'exercise1-description', 'exercise1-manual');
INSERT INTO exercise VALUES(null,2, 'exercise2', 'exercise2-description', 'exercise2-manual');
INSERT INTO exercise VALUES(null,3, 'exercise3', 'exercise3-description', 'exercise3-manual');
INSERT INTO exercise VALUES(null,4, 'exercise4', 'exercise4-description', 'exercise4-manual');
INSERT INTO exercise VALUES(null,5, 'exercise5', 'exercise5-description', 'exercise5-manual');
INSERT INTO exercise VALUES(null,6, 'exercise6', 'exercise6-description', 'exercise6-manual');
INSERT INTO exercise VALUES(null,7, 'exercise7', 'exercise7-description', 'exercise7-manual');
INSERT INTO exercise VALUES(null,8, 'exercise8', 'exercise8-description', 'exercise8-manual');
INSERT INTO exercise VALUES(null,9, 'exercise9', 'exercise9-description', 'exercise9-manual');

INSERT INTO progress VALUES(null,1,1);
INSERT INTO progress VALUES(null,1,2);
INSERT INTO progress VALUES(null,1,3);
INSERT INTO progress VALUES(null,2,4);
INSERT INTO progress VALUES(null,2,5);
INSERT INTO progress VALUES(null,2,6);
INSERT INTO progress VALUES(null,3,7);
INSERT INTO progress VALUES(null,3,8);
INSERT INTO progress VALUES(null,3,9);
