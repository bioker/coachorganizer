CREATE TABLE progress (
	id INTEGER PRIMARY KEY,
	lesson INTEGER,
	exercise INTEGER,
  	FOREIGN KEY (exercise) REFERENCES exercise(id) ,
  	FOREIGN KEY (lesson) REFERENCES lesson(id) 
);

CREATE TABLE lesson (
	id INTEGER PRIMARY KEY,
	myGroup INTEGER,
	lessonDate TEXT NOT NULL,
	beginTime TEXT NOT NULL,
	endTime TEXT NOT NULL,
	price FLOAT DEFAULT 0,
	note TEXT,
  	FOREIGN KEY (myGroup) REFERENCES myGroup(id)
);

CREATE TABLE educable (
	id INTEGER PRIMARY KEY,
	myGroup INTEGER,
	name TEXT NOT NULL,
  	FOREIGN KEY (myGroup) REFERENCES myGroup(id)
);

CREATE TABLE theme (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	description TEXT,
	manual TEXT
);

CREATE TABLE exercise (
	id INTEGER PRIMARY KEY,
	theme INTEGER,
	name TEXT NOT NULL,
	description TEXT,
	manual TEXT,
  	FOREIGN KEY (theme) REFERENCES theme(id)
);

CREATE TABLE presence (
	id INTEGER PRIMARY KEY,
	educable INTEGER,
	lesson INTEGER,
  	FOREIGN KEY (educable) REFERENCES educable(id),
  	FOREIGN KEY (lesson) REFERENCES lesson(id)
);

CREATE TABLE myGroup (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL
);

CREATE TABLE educableInfo (
	id INTEGER PRIMARY KEY,
	educable INTEGER,
	name TEXT NOT NULL,
	info TEXT,
  	FOREIGN KEY (educable) REFERENCES educable(id)
);

CREATE TABLE payment (
	id INTEGER PRIMARY KEY,
	educable INTEGER,
	paymentDate TEXT NOT NULL,
	payment FLOAT NOT NULL,
  	FOREIGN KEY (educable) REFERENCES educable(id)
);
